set -o vi

complete -cf sudo

source /usr/share/bash-completion/bash_completion

[ -f ~/.bash_aliases ] && source ~/.bash_aliases
for f in ~/.bash.d/*; do source $f; done

export EDITOR=nvim

if [[ -v XDG_RUNTIME_DIR ]] && [ -S $XDG_RUNTIME_DIR/gnupg/S.gpg-agent.ssh ]; then
  unset SSH_AGENT_PID
  export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
  export GPG_TTY=$(tty)
  gpg-connect-agent updatestartuptty /bye >/dev/null
fi

[ -f ~/.bash_local ] && source ~/.bash_local

source ~/.bash_alias_completion

SEAFLY_LAYOUT=2
SEAFLY_MULTILINE=1
SEAFLY_SHOW_HOST=0
PROMPT_DIRTRIM=3
source ~/.bash-seafly-prompt/command_prompt.bash

export PATH=$HOME/.local/bin:$PATH
