# Weechat config

## References

https://gist.github.com/pascalpoitras/8406501

## Enable Mouse

`/mouse enable`

## Configure Security

`/secure passphrase <PASSWORD>`

`/secure set freenodepass <PASSWORD>`

## Default settings

```
/set irc.server_default.sasl_mechanism PLAIN
/set irc.server_default.sasl_username <USERNAME>
/set irc.server_default.nicks <USERNAME>
/set irc.server_default.capabilities "account-notify,away-notify,cap-notify,multi-prefix,server-time,znc.in/self-message"
```

## Network settings

```
/server add freenode chat.freenode.net/6697 -ssl -autoconnect
/set irc.server.freenode.sasl_password ${sec.data.freenodepass}

/connect -auto
```

## Plugins

### Load at start up

Load all execpt:

```
/set weechat.plugin.autoload "*,!guile,!javascript,!lua,!php,!tcl,!perl,!ruby,!fifo,!xfer"
/script install autojoin.py autosort.py
```

### Spelling

```
/set spell.check.default_dict en
/set spell.check.suggestions 3
/set spell.color.suggestion *green
/spell enable
```
