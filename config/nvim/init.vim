source ~/.config/nvim/plugins.vim

" Set up leader
let mapleader=","

" save buffer on losing focus and leaving buffer
au FocusLost * :wa
au BufLeave * :wa

au BufNewFile,BufRead /dev/shm/gopass.* setlocal noswapfile nobackup noundofile

lua require 'init'

"
" " ========== Vim-Plug ========== "

" silent function! InitVimPlug(path)
  " let plug = "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"
  " if empty(glob(a:path))
    " silent execute "!curl -fLo " . a:path . " --create-dirs " . plug
    " autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
  " endif
" endfunction

" if has('nvim')
    " call InitVimPlug('~/.local/share/nvim/site/autoload/plug.vim')
    " call plug#begin('~/.config/nvim/plugged')
" else
    " InitVimPlug('~/.vim/autoload/plug.vim')
    " call plug#begin('~/.vim/plugged')
" endif

" " ========== UI and colourschemes ========== "

" Plug 'christoomey/vim-tmux-navigator'
" Plug 'easymotion/vim-easymotion'
" Plug 'edkolev/tmuxline.vim'
" Plug 'gregsexton/MatchTag'
" Plug 'arcticicestudio/nord-vim'
" Plug 'junegunn/goyo.vim'
" Plug 'luochen1990/rainbow'
" Plug 'ryanoasis/vim-devicons'
" Plug 'vim-airline/vim-airline'
" Plug 'vim-airline/vim-airline-themes'


" " ========== Functionality ========== "

" " Languages
" Plug 'pangloss/vim-javascript'
" Plug 'elzr/vim-json'
" Plug 'modille/groovy.vim'
" Plug 'mustache/vim-mustache-handlebars'
" Plug 'mxw/vim-jsx'
" Plug 'ekalinin/Dockerfile.vim'
" Plug 'hashivim/vim-terraform'
" Plug 'PProvost/vim-ps1'
" Plug 'kovetskiy/sxhkd-vim'
" Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() } }
" Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
" Plug 'alvan/vim-closetag'

" " Completion, linters, LSP
" Plug 'jaawerth/nrun.vim'
" Plug 'dense-analysis/ale'
" Plug 'neoclide/coc.nvim', {'branch': 'release'}

" " Visual
" Plug 'google/vim-searchindex'
" Plug 'Yggdroot/indentLine'
" Plug 'mhinz/vim-startify'
" Plug 'romainl/vim-cool'
" Plug 'liuchengxu/vista.vim'

" " Snippets
" Plug 'SirVer/ultisnips'
" Plug 'honza/vim-snippets'

" " Search
" Plug 'mhinz/vim-grepper'
" Plug 'junegunn/fzf.vim'

" " Other
" Plug 'editorconfig/editorconfig-vim'
" Plug 'scrooloose/nerdcommenter'
" Plug 'scrooloose/nerdtree'
" Plug 'machakann/vim-sandwich'
" Plug 'jiangmiao/auto-pairs'
" Plug 'airblade/vim-gitgutter'
" Plug 'ervandew/supertab'
" " Plug 'mileszs/ack.vim'
" " Plug 'moll/vim-bbye'
" " Plug 'roxma/python-support.nvim'
" " Plug 'roxma/nvim-cm-tern', {'do': 'npm install'}
" " Plug 'vim-scripts/Decho'
" Plug 'tpope/vim-fugitive'
" " Plug 'majutsushi/tagbar'


" call plug#end()


" " ========== General ========== "
" syntax enable " syntax on
" filetype indent on
" set autoread " Set to auto read when a file is changed from the outside
" set number " use line nummber and not relative line number
" set norelativenumber
" set scrolloff=8 " start scrolling a number of lines from the edge
" set smartcase " ignore case on search unless there is an uppercase letter
" set incsearch " make search act like a modern browser
" set hlsearch " highlight search results
" set ruler " always show current position
" set cursorline " highlight current line
" set colorcolumn=100 " set a marker at column number
" set showmatch " show matching brackets when cursor is over them
" set wildmenu " turn on wildmenu (tab completion on command bar)
" set lazyredraw " don't redraw while executing macros
" set showcmd " show command in last line of the screen
" set backspace=indent,eol,start " make backspace behave properly
" " set nowrap
" set tabstop=2
" set softtabstop=2
" set expandtab " expand tabs to spaces
" set smarttab
" set shiftwidth=2
" set autoindent  " Take indent for new line from previous line
" set smartindent " Enable smart indentation

" " require by coc
" set hidden
" set nobackup
" set nowritebackup
" set cmdheight=2
" set updatetime=300
" set shortmess+=c
" set signcolumn=yes

" " Theme {{{
" " if (has("termguicolors"))
" " set termguicolors
" " endif
" set background=dark
" " let g:one_allow_italics = 1
" " let g:material_style='oceanic'
" colorscheme nord
" " " }}}

" " if has('mouse')
" set mouse=a
" " endif


" " ========== Python Virtualenv ==========""
" let g:python3_host_prog = expand('~/.pyenv/versions/neovim/bin/python')

" " set swap and undo directories
" " if has('nvim')
  " " set backupdir=~/.config/nvim/backup
  " " set directory=~/.config/nvim/backup
  " " set undodir=~/.config/nvim/undodir
" " else
  " " set backupdir=~/.vim/backup
  " " set directory=~/.vim/swap
  " " set undodir=~/.vim/undo
" " endif
" set undodir=~/.nvim/tmp
" set undofile
" " set undolevels=100
" " set undoreload=1000


" " ========== File specific config ========== "
" autocmd BufNewFile,BufRead Jenkinsfile,*.Jenkinsfile set syntax=groovy
" autocmd BufNewFile,BufRead Dockerfile,Dockerfile.* set syntax=Dockerfile
" augroup javascript
    " au FileType go set expandtab
    " au FileType go set shiftwidth=2
    " au FileType go set softtabstop=2
    " au FileType go set tabstop=2
" augroup END

" augroup python
    " au FileType go set expandtab
    " au FileType go set shiftwidth=4
    " au FileType go set softtabstop=4
    " au FileType go set tabstop=4
" augroup END

" augroup golang
    " au FileType go set noexpandtab
    " au FileType go set shiftwidth=4
    " au FileType go set softtabstop=4
    " au FileType go set tabstop=4
" augroup END


" " ========== Plugin Configuration ========== "

" " ALE ========== "
" let g:ale_completion_enabled = 1
" let g:ale_sign_column_always = 1
" let g:ale_sign_error = '✗'
" let g:ale_sign_warning = '⚠'
" let g:ale_sign_info = ''
" " brew install shellcheck shfmt
" " let g:ale_linters = {
" " \   'sh': ['shellcheck'],
" " \   'go': ['gopls'],
" " \   'javascript': ['eslint', 'standard'],
" " \   'python': ['flake8', 'pylint'],
" " \}
" let g:ale_fixers = {
" \   'sh': ['shfmt'],
" \   'javascript': ['eslint'],
" \   'python': [
" \       'add_blank_lines_for_python_control_statements',
" \       'remove_trailing_lines',
" \       'trim_whitespace',
" \       'autopep8',
" \       'yapf',
" \   ],
" \}
" let g:ale_fix_on_save = 1
" " ALE - Language specific
" let g:ale_python_auto_pipenv = 1
" au BufRead,BufEnter *.js let b:ale_javascript_standard_executable = nrun#Which('semistandard')

" " Airline ========= "
" let g:airline_theme='nord'
" let g:airline#extensions#tabline#enabled = 1
" let g:airline#extensions#tabline#show_tabs = 0
" let g:airline#extensions#ale#enabled = 1
" let g:airline#extensions#vista#enabled = 1
" let g:airline_powerline_fonts = 1

" " Coc
" " list of the extensions required
" let g:coc_global_extensions = [
            " \'coc-yank',
            " \'coc-pairs',
            " \'coc-json',
            " \'coc-css',
            " \'coc-html',
            " \'coc-tsserver',
            " \'coc-yaml',
            " \'coc-lists',
            " \'coc-snippets',
            " \'coc-ultisnips',
            " \'coc-python',
            " \'coc-xml',
            " \'coc-syntax',
            " \'coc-gocode',
            " \]
" " use tab for completion trigger
" " inoremap <silent><expr> <TAB>
      " " \ pumvisible() ? "\<C-n>" :
      " " \ <SID>check_back_space() ? "\<TAB>" :
      " " \ coc#refresh()
" " inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

" " " Navigate snippet placeholders using tab
" " let g:coc_snippet_next = '<Tab>'
" " let g:coc_snippet_prev = '<S-Tab>'

" " " use tab for completion trigger
" " inoremap <silent><expr> <TAB>
      " " \ pumvisible() ? "\<C-n>" :
      " " \ <SID>check_back_space() ? "\<TAB>" :
      " " \ coc#refresh()
" " inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

" " Navigate snippet placeholders using tab
" let g:coc_snippet_next = '<Tab>'
" let g:coc_snippet_prev = '<S-Tab>'

" " Easymotion ========== "
" let g:EasyMotion_startofline = 0
" let g:EasyMotion_smartcase = 1

" " Editorconfig ========== "
" let g:EditorConfig_exclude_patterns = ['fugitive://.*']

" " FZF
" if filereadable("/usr/local/opt/fzf/bin/fzf")
  " set rtp+=/usr/local/opt/fzf
" else
  " set rtp+=
" endif
" let g:fzf_layout = { 'window': 'call CreateCenteredFloatingWindow()' }
" let g:fzf_commits_log_options = '--graph --color=always
  " \ --format="%C(yellow)%h%C(red)%d%C(reset)
  " \ - %C(bold green)(%ar)%C(reset) %s %C(blue)<%an>%C(reset)"'
" " Indent Line ========== "
" let g:indentLine_char = "| "
" let g:indentLine_color_gui = "#363949"

" " Rainbow brackets ========== "
" let g:rainbow_active = 1

" " Startify ========== "
" let g:startify_session_persistence = 1

" " Tagbar ========== "
" let g:tagbar_autofocus = 1


" " NERDcommenter
" let g:NERDSpaceDelims = 1

" " NERDTree ========== "
" map <C-e> :NERDTreeToggle<Cr>
" let NERDTreeShowHidden = 1
" let NERDTreeIgnore = ['^\.git$', 'node_modules', 'logs', '\.log']
" autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q |endif
"

" " supertab
" let g:SuperTabDefaultCompletionType = "<C-n>"
" let g:SuperTabClosePreviewOnPopupClose = 1

" " bbye
" map <Leader>d :Bdelete<Cr>

" " vim-jsx
" let g:jsx_ext_required = 0

" " Vim-Go ========== "
" let g:go_def_mapping_enabled = 0

" " Vim-terraform ========== "
" let g:terraform_fmt_on_save = 1

" " Yggdroot/indentLine
" let g:vim_json_syntax_conceal = 0



" " 'majutsushi/tagbar' {{{
  " " On Mac OS `brew install ctags`
  " " go get -u github.com/jstemmer/gotags
  " " Add shortcut for toggling the tag bar
  " " nnoremap <F3> :TagbarToggle<cr>

  " " Language: Go
  " " Tagbar configuration for Golang
  " " let g:tagbar_type_go = {
      " " \ 'ctagstype' : 'go',
      " " \ 'kinds'     : [
          " " \ 'p:package',
          " " \ 'i:imports:1',
          " " \ 'c:constants',
          " " \ 'v:variables',
          " " \ 't:types',
          " " \ 'n:interfaces',
          " " \ 'w:fields',
          " " \ 'e:embedded',
          " " \ 'm:methods',
          " " \ 'r:constructor',
          " " \ 'f:functions'
      " " \ ],
      " " \ 'sro' : '.',
      " " \ 'kind2scope' : {
          " " \ 't' : 'ctype',
          " " \ 'n' : 'ntype'
      " " \ },
      " " \ 'scope2kind' : {
          " " \ 'ctype' : 't',
          " " \ 'ntype' : 'n'
      " " \ },
      " " \ 'ctagsbin'  : 'gotags',
      " " \ 'ctagsargs' : '-sort -silent'
  " " \ }
" " " }}}


" " ========== Custom Functions ========== "
" " markdown files preview inside (you need to install mdv)
" function! TerminalPreviewMarkdown()
    " vsp | terminal ! mdv %
" endfu

" " tabs manipulation
" function! Rotate() " switch between horizontal and vertical split mode for open splits
    " " save the original position, jump to the first window
    " let initial = winnr()
    " exe 1 . "wincmd w"

    " wincmd l
    " if winnr() != 1
        " " succeeded moving to the right window
        " wincmd J                " make it the bot window
    " else
        " " cannot move to the right, so we are at the top
        " wincmd H                " make it the left window
    " endif

    " " restore cursor to the initial window
    " exe initial . "wincmd w"
" endfunction

" nnoremap <F5> :call Rotate()<CR>

" " floating fzf window with borders
" function! CreateCenteredFloatingWindow()
    " let width = min([&columns - 4, max([80, &columns - 20])])
    " let height = min([&lines - 4, max([20, &lines - 10])])
    " let top = ((&lines - height) / 2) - 1
    " let left = (&columns - width) / 2
    " let opts = {'relative': 'editor', 'row': top, 'col': left, 'width': width, 'height': height, 'style': 'minimal'}

    " let top = "╭" . repeat("─", width - 2) . "╮"
    " let mid = "│" . repeat(" ", width - 2) . "│"
    " let bot = "╰" . repeat("─", width - 2) . "╯"
    " let lines = [top] + repeat([mid], height - 2) + [bot]
    " let s:buf = nvim_create_buf(v:false, v:true)
    " call nvim_buf_set_lines(s:buf, 0, -1, v:true, lines)
    " call nvim_open_win(s:buf, v:true, opts)
    " set winhl=Normal:Floating
    " let opts.row += 1
    " let opts.height -= 2
    " let opts.col += 2
    " let opts.width -= 4
    " call nvim_open_win(nvim_create_buf(v:false, v:true), v:true, opts)
    " au BufWipeout <buffer> exe 'bw '.s:buf
" endfunction

" " Files + devicons + floating fzf
" function! Fzf_dev()
  " let l:fzf_files_options = '--preview "bat --theme="OneHalfDark" --style=numbers,changes --color always {2..-1} | head -'.&lines.'"'
  " function! s:files()
    " let l:files = split(system($FZF_DEFAULT_COMMAND), '\n')
    " return s:prepend_icon(l:files)
  " endfunction

  " function! s:prepend_icon(candidates)
    " let l:result = []
    " for l:candidate in a:candidates
      " let l:filename = fnamemodify(l:candidate, ':p:t')
      " let l:icon = WebDevIconsGetFileTypeSymbol(l:filename, isdirectory(l:filename))
      " call add(l:result, printf('%s %s', l:icon, l:candidate))
    " endfor

    " return l:result
  " endfunction

  " function! s:edit_file(item)
    " let l:pos = stridx(a:item, ' ')
    " let l:file_path = a:item[pos+1:-1]
    " execute 'silent e' l:file_path
  " endfunction

  " call fzf#run({
        " \ 'source': <sid>files(),
        " \ 'sink':   function('s:edit_file'),
        " \ 'options': '-m --reverse ' . l:fzf_files_options,
        " \ 'down':    '40%',
        " \ 'window': 'call CreateCenteredFloatingWindow()'})

" endfunction

" " ---------- Custom Mappings ---------- "

" " disable arrow keys in normal mode
" noremap <Up> <NOP>
" noremap <Down> <NOP>
" noremap <Left> <NOP>
" noremap <Right> <NOP>

" let mapleader = ',' " change the Leader key for quick commands
" "
" " change <Esc> key
" :imap jk <Esc>

" " quick save
" map <Leader>w :w<Cr>

" " copy and paste to clipboard
" vnoremap <Leader>y  "+y
" nnoremap <Leader>Y  "+yg_
" nnoremap <Leader>y  "+y
" nnoremap <Leader>p "+p
" nnoremap <Leader>P "+P
" vnoremap <Leader>p "+p
" vnoremap <Leader>P "+P

" " edit and source vimrc and system configs
" map <Leader>sv :source $MYVIMRC<Cr>
" map <Leader>ev :e $MYVIMRC<Cr>
" map <Leader>ez :e ~/.zshrc<Cr>
" map <Leader>eb :e ~/.bashrc<Cr>
" map <Leader>et :e ~/.tmux.conf<Cr>

" " quick turn off search highlight
" " map <Leader>h :nohl<Cr>

" " Coc
" "" coc mappings

" " multi cursor shortcuts
" nmap <silent> <C-c> <Plug>(coc-cursors-position)
" nmap <silent> <C-a> <Plug>(coc-cursors-word)
" xmap <silent> <C-a> <Plug>(coc-cursors-range)

" " Use `[g` and `]g` to navigate diagnostics
" nmap <silent> [g <Plug>(coc-diagnostic-prev)
" nmap <silent> ]g <Plug>(coc-diagnostic-next)

" " for global rename
" nmap <leader>rn <Plug>(coc-rename)

" " new line in normal mode and back
" map <Enter> o<ESC>
" map <S-Enter> O<ESC>

" " FZF
" nnoremap <silent> <Space><Space> :Files<cr>
" nnoremap <silent> <Space>- :Files <C-r>=expand("%:h")<cr>/<cr>
" nnoremap <silent> <Space>b :Buffers<cr>
" nnoremap <silent> <Space>] :Tags<cr>
" nnoremap <silent> <Space>]b :BTags<cr>
" nnoremap <silent> <Space>c :Commits<cr>
" nnoremap <silent> <Space>cb :BCommits<cr>
" nnoremap <silent> <Space>l :Lines<cr>
" nnoremap <silent> <Space>lb :BLines<cr>
" nnoremap <silent> <Space>m :Marks<cr>
" nnoremap <silent> <Space>s :Snippets<cr>
" nnoremap <silent> <Space>mp :Maps<cr>
" nnoremap <silent> <Space>h :Helptags<cr>
" nnoremap <silent> <Space>cl :Colors<cr>
" nnoremap <C-f> :Rg<Space>
" nnoremap <leader>!  :Rg!<Space>


" " save buffer on losing focus and leaving buffer
" au FocusLost * :wa
" au BufLeave * :wa

