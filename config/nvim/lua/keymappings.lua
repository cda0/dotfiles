local remap = vim.api.nvim_set_keymap

-- Remap escape keys to something usable on home row
remap('i', 'jk',    '<Esc>', { noremap = true })
remap('c', 'jk',    '<C-C>', { noremap = true })
remap('i', '<Esc>', '<Nop>', { noremap = true })
remap('c', '<Esc>', '<Nop>', { noremap = true })

-- Remap return in normal mode to be a CR
remap('n', '<Enter>', 'o<Esc>', { noremap = true })
remap('n', '<S-Enter>', 'O<Esc>', { noremap = true })

-- Copy and paste
remap('v', '<Leader>y', '"+y', { noremap = true })
remap('n', '<Leader>y', '"+y', { noremap = true })
remap('n', '<Leader>Y', '"+yg_', { noremap = true })
remap('v', '<Leader>p', '"+p', { noremap = true })
remap('v', '<Leader>P', '"+P', { noremap = true })
remap('n', '<Leader>p', '"+p', { noremap = true })
remap('n', '<Leader>P', '"+P', { noremap = true })

-- Quick save
remap('n', '<Leader>w', ':w<Cr>', { noremap = true })

-- Disable Arrow Keys
remap('i', '<Up>',    '<NOP>', { noremap = true })
remap('i', '<Down>',  '<NOP>', { noremap = true })
remap('i', '<Left>',  '<NOP>', { noremap = true })
remap('i', '<Right>', '<NOP>', { noremap = true })
remap('n', '<Up>',    '<NOP>', { noremap = true })
remap('n', '<Down>',  '<NOP>', { noremap = true })
remap('n', '<Left>',  '<NOP>', { noremap = true })
remap('n', '<Right>', '<NOP>', { noremap = true })

