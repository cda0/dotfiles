vim.g.airline_theme = 'nord'
vim.g['airline#extensions#tabline#enabled'] = 1
vim.g['airline#extensions#tabline#show_tabs'] = 0
vim.g['airline#extensions#nvimlsp#enabled'] = 0
vim.g['airline#extensions#vista#enabled'] = 1
vim.g.airline_powerline_fonts = 1
