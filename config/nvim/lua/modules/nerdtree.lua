local remap = vim.api.nvim_set_keymap

vim.g.NERDTreeShowHidden = 1
vim.g.NERDTreeIgnore = { '^.git$', 'node_modules', 'logs', '.log' }

remap('n', '<C-e>', ':NERDTreeToggle<Cr>', { noremap = true })
vim.api.nvim_command('autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q |endif')
