vim.g.completion_enable_auto_popup = 1
vim.g.completion_auto_change_source = 1
vim.g.completion_trigger_on_delete = 1
vim.g.completion_matching_ignore_case = 1
vim.g.completion_matching_strategy_list = { 'exact', 'substring' }

