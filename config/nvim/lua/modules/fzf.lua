local remap = vim.api.nvim_set_keymap

vim.g.fzf_layout = {
  window = {
    width = 0.8,
    height = 0.8
  },
}

remap('n', '<Leader>b',  ':Buffers<CR>',  { noremap = true })
remap('n', '<Leader>B',  ':History<CR>',  { noremap = true })
remap('n', '<Leader>/',  ':GFiles?<CR>',  { noremap = true })
remap('n', '<Leader>ff', ':GFiles<CR>',   { noremap = true })
remap('n', '<Leader>fF', ':Files<CR>',    { noremap = true })
remap('n', '<Leader>fL', ':Lines<CR>',    { noremap = true })
remap('n', '<Leader>fc', ':BCommits<CR>', { noremap = true })
remap('n', '<Leader>fC', ':Commits<CR>',  { noremap = true })
remap('n', '<Leader>fh', ':History:<CR>', { noremap = true })
remap('n', '<Leader>fH', ':History/<CR>', { noremap = true })
remap('n', '<Leader>fm', ':Commands<CR>', { noremap = true })
remap('n', '<Leader>fo', ':Locate<CR>',   { noremap = true })
remap('n', '<Leader>fk', ':Maps<CR>',     { noremap = true })
remap('n', '<Leader>f/', ':Rg<CR>',       { noremap = true })
remap('n', '<Leader>fs', ':exe \':Rg \' . expand(\'<cword>\')<CR>', { noremap = true })

remap('i', '<C-x><C-w>', '<Plug>(fzf-complete-word)', { noremap = false })
remap('i', '<C-x><C-p>', '<Plug>(fzf-complete-path)', { noremap = false })
remap('i', '<C-x><C-f>', '<Plug>(fzf-complete-file)', { noremap = false })
remap('i', '<C-x><C-l>', '<Plug>(fzf-complete-line)', { noremap = false })

