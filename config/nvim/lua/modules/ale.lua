vim.g.ale_completion_enabled = 0
vim.g.ale_sign_column_always = 1
vim.g.ale_sign_error = '✗'
vim.g.ale_sign_warning = '⚠'
vim.g.ale_sign_info = ''
vim.g.ale_fix_on_save = 1
vim.g.ale_linters = {
  sh = {'shellcheck'},
  go = {'gopls'},
  javascript = {'eslint'},
  python = {'flake8', 'pylint'},
  terraform = {'tflint'},
}
vim.g.ale_fixers = {
 sh = {'shfmt'},
 javascript = {'eslint'},
 python = {
     'add_blank_lines_for_python_control_statements',
     'remove_trailing_lines',
     'trim_whitespace',
     'autopep8',
     'yapf',
 },
}
