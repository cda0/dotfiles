local nvim_command = vim.api.nvim_command

-- Global vim options
vim.o.autochdir          = false
vim.o.autoindent         = true
vim.o.autoread           = true
vim.o.backup             = false
vim.o.errorbells         = false
vim.o.encoding           = 'utf-8'
vim.o.exrc               = true
vim.o.expandtab          = true
vim.o.fileformat         = 'unix'
vim.o.foldenable         = false
vim.o.hidden             = true
vim.o.hlsearch           = true
vim.o.ignorecase         = true
vim.o.inccommand         = 'split'
vim.o.joinspaces         = false
vim.o.laststatus         = 2
vim.o.linespace          = 0
vim.o.magic              = true
vim.o.modeline           = true
vim.o.ruler              = true
vim.o.scrolloff          = 3
vim.o.secure             = true
vim.o.showcmd            = true
vim.o.showmatch          = true
vim.o.showmode           = false
vim.o.shiftwidth         = 2
vim.o.sidescrolloff      = 5
vim.o.smartcase          = true
vim.o.smarttab           = true
vim.o.smartindent        = true
vim.o.softtabstop        = 2
vim.o.splitbelow         = true
vim.o.splitright         = true
vim.o.startofline        = false
vim.o.swapfile           = false
vim.o.switchbuf          = 'useopen'
vim.o.tabstop            = 2
vim.o.textwidth          = 78
vim.o.termguicolors      = true
vim.o.undofile           = true
vim.o.updatetime         = 100
vim.o.writebackup        = false

nvim_command('set completeopt=menuone,noinsert,noselect')
nvim_command('set shortmess+=c')
nvim_command('set mouse=a')
nvim_command('set wildmenu')
nvim_command('set backspace=indent,eol,start')
nvim_command('set number')
nvim_command('set colorcolumn=100')
nvim_command('set cursorline')
nvim_command('set termguicolors')
nvim_command('syntax enable')


require 'keymappings'
require 'lsp'
require 'treesitter'

require 'modules.airline'
require 'modules.ale'
require 'modules.better-whitespace'
require 'modules.completion'
require 'modules.editorconfig'
require 'modules.fzf'
require 'modules.go'
require 'modules.indentLine'
require 'modules.nerdcommenter'
require 'modules.nerdtree'
require 'modules.nord'
require 'modules.rainbow'
require 'modules.terraform'

vim.cmd [[colorscheme nord]]

