local nvim_lsp = require('lspconfig')
local completion = require('completion')
local lsp_status = require('lsp-status')
local remap = vim.api.nvim_set_keymap


local on_attach = function(client, bufnr)
  vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')
  completion.on_attach(client, bufnr)
  lsp_status.on_attach(client, bufnr)

end

vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(
  vim.lsp.diagnostic.on_publish_diagnostics, {
    virtual_text = false,
    signs = true,
    update_in_insert = false,
    underline = true,
  }
)

lsp_status.register_progress()
lsp_status.config({
  status_symbol = '',
  indicator_errors = 'e',
  indicator_warnings = 'w',
  indicator_info = 'i',
  indicator_hint = 'h',
  indicator_ok = '✔️',
  spinner_frames = { '⣾', '⣽', '⣻', '⢿', '⡿', '⣟', '⣯', '⣷' },
})

nvim_lsp.tsserver.setup {
  on_attach = on_attach,
  capabilities = lsp_status.capabilities
}

nvim_lsp.gopls.setup {
  on_attach = on_attach,
  capabilities = lsp_status.capabilities
}

nvim_lsp.terraformls.setup {
  on_attach = on_attach,
  capabilities = lsp_status.capabilities
}

-- github.com/nvim-lua/completion-nvim/issues/62
vim.g.completion_confirm_key = ""
remap('i', '<Cr>', 'pumvisible() ? "<Plug>(completion_confirm_completion)" : "<cr>"',   { noremap = true, expr = true })

-- Mappings for diagnostics
remap('n', '[d', '<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>', { noremap = true })
remap('n', ']d', '<cmd>lua vim.lsp.diagnostic.goto_next()<CR>', { noremap = true })
remap('n', '[D', '<cmd>lua vim.lsp.diagnostic.goto_prev { wrap = false }<CR>', { noremap = true })
remap('n', ']D', '<cmd>lua vim.lsp.diagnostic.goto_next { wrap = false }<CR>', { noremap = true })

-- Mappings for lsp
local opts = { noremap=true, silent=true }
remap('n', 'gd',         '<cmd>lua vim.lsp.buf.definition()<CR>', opts)
remap('n', 'K',          '<cmd>lua vim.lsp.buf.hover()<CR>', opts)
remap('n', 'gr',         '<cmd>lua vim.lsp.buf.references()<CR>', opts)
remap('n', 'ga',         '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)
remap('n', 'gD',         '<cmd>lua vim.lsp.util.show_line_diagnostics()<CR>', opts)
remap('n', 'gi',         '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
remap('n', 'gs',         '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
remap('n', 'gt',         '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
remap('n', 'g0',         '<cmd>lua vim.lsp.buf.document_symbol()<CR>', opts)
remap('n', 'gW',         '<cmd>lua vim.lsp.buf.workspace_symbol()<CR>', opts)
remap('n', '<Leader>de', '<cmd>lua vim.lsp.buf.declaration()<CR>', opts)
remap('n', '<Leader>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
