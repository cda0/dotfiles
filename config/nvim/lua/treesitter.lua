require'nvim-treesitter.configs'.setup {
  ensure_installed = { "go", "javascript", "lua" },
  highlight = {
    enable = true,
  },
}

