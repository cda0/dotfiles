silent function! InitVimPlug(path)
  let plug = "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"
  if empty(glob(a:path))
    silent execute "!curl -fLo " . a:path . " --create-dirs " . plug
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
  endif
endfunction

call InitVimPlug('~/.local/share/nvim/site/autoload/plug.vim')

call plug#begin('~/.local/share/nvim/site/plugged')

" -- Language Server, linting, etc
Plug 'dense-analysis/ale'
Plug 'neovim/nvim-lspconfig'
Plug 'nvim-lua/completion-nvim'
Plug 'nvim-lua/lsp-status.nvim'
Plug 'nvim-treesitter/nvim-treesitter'

" -- Filetype support
Plug 'pangloss/vim-javascript'
Plug 'elzr/vim-json'
Plug 'tpope/vim-markdown'
Plug 'hashivim/vim-terraform'
Plug 'modille/groovy.vim'
Plug 'mustache/vim-mustache-handlebars'
Plug 'PProvost/vim-ps1'
Plug 'kovetskiy/sxhkd-vim'
Plug 'fatih/vim-go'

" -- UI
Plug 'christoomey/vim-tmux-navigator'
Plug 'edkolev/tmuxline.vim'
Plug 'ryanoasis/vim-devicons'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'arcticicestudio/nord-vim'
Plug 'preservim/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'

" -- Search
Plug 'mhinz/vim-grepper'
Plug 'junegunn/fzf.vim'

Plug 'editorconfig/editorconfig-vim'

Plug 'airblade/vim-gitgutter'
Plug 'rhysd/git-messenger.vim'
Plug 'tpope/vim-fugitive'

Plug 'romainl/vim-cool' " -- Disables search highlighting

Plug 'justinmk/vim-sneak'

Plug 'tpope/vim-surround'

Plug 'psliwka/vim-smoothie'

Plug 'Yggdroot/indentLine'

Plug 'preservim/nerdcommenter'

Plug 'jiangmiao/auto-pairs'

Plug 'luochen1990/rainbow'

Plug 'ntpeters/vim-better-whitespace'

Plug 'vimwiki/vimwiki'

call plug#end()
