###########
# Windows #
###########

# Close or kill window
super + shift + {_,ctrl + }q
	bspc node -{c,k}
# Focus or swap window in the current workspace
super + {_,shift + }{h,j,k,l}
	bspc node -{f,s} {west,south,north,east}
# Focus the given workspace or move window to that workspace
super + {_,shift + }{1-9,0}
	bspc {desktop -f, node -d} '^{1-9,10}'
# Focus the next/previous window
super + {_,shift + }grave
	bspc node -f {next,prev}.local
# Focus the last workspace
super + Tab
	bspc desktop -f last
# Go to next desktop
super + {Left, Right}
	bspc desktop -f {prev, next}
# move focused window to the next workspace and then switch to that workspace
super + shift + {Left,Right}
	id=$(bspc query --nodes --node); bspc node --to-desktop {prev,next}; bspc desktop --focus next; bspc node --focus ${id}

#############
# Preselect #
#############

# Preselect the direction
super + p ; {h,j,k,l}
	bspc node -p {west,south,north,east}
# Cancel the preselection for the focused desktop
super + p ; c
	bspc query -N -d | xargs -I id -n 1 bspc node id -p cancel

##############
# Properties #
##############

# Toggle floating state
super + f
    if [ -z "$(bspc query -N -n focused.floating)" ]; then \
        bspc node focused -t floating; \
    else \
        bspc node focused -t tiled; \
    fi
# Toggle sticky flag
super + s
	bspc node -g sticky
# Toggle monocle layout
super + m
    bspc desktop -l next

# PiP
super + b
	bspc node focused -t floating \
  xdotool getactivewindow windowsize 200 100 windowmove 700 900

##############
# Media keys #
##############

# Volume
XF86AudioMute
  pactl set-sink-mute @DEFAULT_SINK@ toggle

{XF86AudioLowerVolume,XF86AudioRaiseVolume}
  pactl set-sink-mute @DEFAULT_SINK@ 0; pactl set-sink-volume @DEFAULT_SINK@ {-5%,+5%}

# Screenshot
{_,ctrl + ,alt + }Print
    maim {_,-s,-i $(xdotool getactivewindow)} --noopengl \
        | xclip -selection clipboard -t image/png -i && \
        notify-send -t 1000 "Sceenshot" "Copied to clipboard"
shift + {_,ctrl + ,alt + }Print
    mkdir -p $HOME/Pictures/Screenshots/ && \
    maim {_,-s,-i $(xdotool getactivewindow)} --noopengl \
        $HOME/Pictures/Screenshots/$(date +screenshot-%Y%m%d-%H%M%S.png) && \
        notify-send -t 1000 "Sceenshot" "Saved in ~/Pictures/Screenshots"

################
# Quick launch #
################

# Run command
super + space
    rofi -show combi
# Terminal
super + {_,ctrl +,shift + }Return
	{alacritty,kitty,urxvtc}
# Web browser
super + {_,shift + }w
    firefox {_, --private-window}
# Weechat
super + {r,y}
	  ${TERMINAL} -e {weechat,spt}
# Power menu
super + shift + Escape
		$HOME/.config/bspwm/logout.sh
# Lock screen
super + BackSpace
    betterlockscreen -l blur

alt + shift + 4
	maim -s ~/screen-$(date +%F-%T).png

##############
# System
##############

# Restart Services
super + Escape ; {x,p}
	{killall sxhkd && notify-send "sxhkd reloaded" && sxhkd &, killall polybar && ~/.config/polybar/launch.sh}

super + shift + c
  toggleprogram "picom" "--experimental-backends"
