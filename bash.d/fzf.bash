
# Setup FZF
export FZF_BASE=/usr/share/fzf
export FZF_DEFAULT_COMMAND='fd --type f --color=never --hidden --exclude .git'
export FZF_ALT_C_COMMAND='fd --type d . --color=never --hidden --exclude .git $HOME'
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
export FZF_CTRL_T_OPTS="--preview 'bat --color=always --line-range :500 {}'"
export FZF_ALT_C_OPTS="--preview 'exa --tree {} | head -100'"
export FZF_DEFAULT_OPTS='
  --height 75% --multi --reverse
  --bind ctrl-f:page-down,ctrl-b:page-up
'
[ -f /usr/share/fzf/completion.bash ] && source /usr/share/fzf/completion.bash
[ -f /usr/share/fzf/key-bindings.bash ] && source /usr/share/fzf/key-bindings.bash

