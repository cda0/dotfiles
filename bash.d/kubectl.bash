# Kubectl
if type kubectl > /dev/null; then
  source <(kubectl completion bash)
fi

alias k=kubectl
alias kgp='kubectl get pods'
alias kgpa='kubectl get pods --all-namespaces'

