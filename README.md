# Dotfiles

Now managed by rcm https://github.com/thoughtbot/rcm

```
git submodule update --init --recursive
```

## Install Antigen
```
curl -L git.io/antigen > antigen.zsh
```

## Weechat
https://www.bfoliver.com/technology/2017/07/15/weechat/

## Link

`rcup -fx README.md`

# Fonts
```
git clone https://github.com/powerline/fonts.git --depth=1
cd fonts
./install.sh
cd ..
rm -rf fonts

git clone https://github.com/ryanoasis/nerd-fonts --depth=1
cd nerd-fonts
./install.sh
cd ..
rm -rf nerd-fonts
```

# Tmux plugins
```
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
```

# Neovim Dependencies
```
pip install --user neovim
pip install --user pynvim
npm install -g neovim
gem install neovim
```

# Jedi for Python
```
pip install --user jedi
```

# Ranger
```
git clone https://github.com/alexanderjeurissen/ranger_devicons
cd ranger_devicons
make install
cd ..
rm -rf ranger_devicons
```
