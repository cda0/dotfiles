# zmodload zsh/zprof
#
# Global Config
#
# module_path=($module_path /usr/local/lib/zpython)
# http://joshldavis.com/2014/07/26/oh-my-zsh-is-a-disease-antigen-is-the-vaccine/
# For sudo-ing aliases
# https://wiki.archlinux.org/index.php/Sudo#Passing_aliases
alias sudo='sudo '

# Ensure languages are set
export LANG=en_GB.UTF-8
export LANGUAGE=en_GB.UTF-8
export LC_ALL=en_GB.UTF-8

export ZSH_CACHE_DIR=$HOME/.zsh

# Ensure editor is set
export EDITOR=nvim

# Setup FZF
export FZF_BASE=$HOME/.fzf
export FZF_DEFAULT_COMMAND='fd --type f --color=never --hidden --exclude .git'
export FZF_ALT_C_COMMAND='fd --type d . --color=never --hidden --exclude .git $HOME'
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
export FZF_CTRL_T_OPTS="--preview 'bat --color=always --line-range :500 {}'"
export FZF_ALT_C_OPTS="--preview 'tree -C {} | head -100'"
export FZF_DEFAULT_OPTS='
  --height 75% --multi --reverse
  --bind ctrl-f:page-down,ctrl-b:page-up
'

# Load Antigen
source ~/.antigen.zsh

#
# oh-my-zsh library
#

antigen use oh-my-zsh

#
# Bundles from the default repo (robbyrussel/oh-my-zsh)
#

antigen bundle brew
antigen bundle command-not-found
antigen bundle common-aliases
antigen bundle docker-compose
antigen bundle fzf
antigen bundle git
antigen bundle python
antigen bundle sudo
antigen bundle vi-mode

#
# Antigen Bundles
#

antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-completions
export NVM_AUTO_USE=true
export NVM_NO_USE=false
export NVM_LAZY_LOAD=false
antigen bundle lukechilds/zsh-nvm
antigen bundle lukechilds/zsh-better-npm-completion
antigen bundle mattberther/zsh-pyenv
antigen bundle mattberther/zsh-rbenv
antigen bundle cda0/zsh-tfenv
antigen bundle cda0/zsh-gvm

#
# Antigen theme
#

fpath+=($HOME/.zsh/pure)
autoload -Uz promptinit; promptinit; prompt pure
antigen bundle mafredri/zsh-async
antigen bundle sindresorhus/pure
# export POWERLEVEL9K_INSTALLATION_PATH=$ANTIGEN_BUNDLES/bhilburn/powerlevel9k
# export POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=('vi_mode' 'pyenv' 'virtualenv' 'date' 'time' 'status') 
# antigen theme bhilburn/powerlevel9k powerlevel9k

antigen apply

#
# Nice aliases
#

alias dotfiles="cd ~/.dotfiles"

alias rla="source ~/.zshrc"
alias ezr="$EDITOR ~/.zshrc"

alias antup="curl -L git.io/antigen > ~/antigen.zsh"

alias genssh="ssh-keygen -o -a 100 -t ed25519"

if type exa > /dev/null; then
  alias l='exa --icons'
  alias ls='exa --icons'
  alias ll='exa -l --icons'
  alias la='exa -al --icons'
  alias lh='exa -alh --icons'
  alias lt='exa -T --icons'
  alias llfu='exa -bghHliS --git --icons'
  alias tree='exa -a --tree --git -I .git'
fi

alias oft='onefetch'

alias docker-less='docker ps | less -SEX'
alias docker-kill='docker kill $(docker ps -q)'
alias docker-rm='docker rm $(docker ps -a -q)'
alias docker-rmi='docker rmi $(docker images -q)'
alias docker-rmv='docker volume ls -qf dangling=true | xargs -r docker volume rm'
alias dockers='docker ps --format "table {{.Names}}\\t{{.Image}}\\t{{.Status}}\\t{{.Ports}}\\t{{.Command}}"'
alias dockeri='docker images'

[ -f /usr/share/fzf/completion.zsh ] && source /usr/share/fzf/completion.zsh
[ -f /usr/share/fzf/key-bindings.zsh ] && source /usr/share/fzf/key-bindings.zsh
[ -f /usr/local/opt/fzf/shell/completion.zsh ] && source /usr/local/opt/fzf/shell/completion.zsh
[ -f /usr/local/opt/fzf/shell/key-bindings.zsh ] && source /usr/local/opt/fzf/shell/key-bindings.zsh
[ -f $HOME/.fzf/shell/completion.zsh ] && source $HOME/.fzf/shell/completion.zsh
[ -f $HOME/.fzf/shell/key-bindings.zsh ] && source $HOME/.fzf/shell/key-bindings.zsh

tm() {
  [[ -n "$TMUX" ]] && change="switch-client" || change="attach-session"
  if [ $1 ]; then 
     tmux $change -t "$1" 2>/dev/null || (tmux new-session -d -s $1 && tmux $change -t "$1"); return
  fi
  session=$(tmux list-sessions -F "#{session_name}" 2>/dev/null | fzf --exit-0) &&  tmux $change -t "$session" || echo "No sessions found."
}

dockertags() {
  if [ $# -lt 1 ]; then
  cat << HELP

dockertags -- list all tags for a Docker image on a remote registry.

EXAMPLE:
  - list all tags for ubuntu:
    dockertags ubuntu

  - list all php tags containing apache:
    dockertags php apache

HELP
  fi

  image="$1"
  tags=`wget -q https://registry.hub.docker.com/v1/repositories/${image}/tags -O - | sed -e 's/[][]//g' -e 's/"//g' -e 's/ //g' | tr '}' '\n' | awk -F: '{print $3}'`

  if [ -n "$2" ]; then
    tags=`echo "${tags}" | grep "$2"`
  fi

  echo "${tags}"
}

# rootless dockerconfig
if [[ -v XDG_RUNTIME_DIR ]] && [ -S $XDG_RUNTIME_DIR/docker.sock ]; then
  export DOCKER_HOST=unix://$XDG_RUNTIME_DIR/docker.sock
fi


if [[ -v XDG_RUNTIME_DIR ]] && [ -S $XDG_RUNTIME_DIR/gnupg/S.gpg-agent.ssh ]; then
  unset SSH_AGENT_PID
  export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
  export GPG_TTY=$(tty)
  gpg-connect-agent updatestartuptty /bye >/dev/null
fi

export PATH=$HOME/.local/bin:$HOME/.bin:$PATH

#
# Work specific config
#
[ -f ~/.sh_local ] && source ~/.sh_local

