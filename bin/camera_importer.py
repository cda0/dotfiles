#!/usr/bin/env python

import argparse
import datetime
import json
import os
import pathlib
import subprocess

#  PHOTOS_DIR = os.path.join(pathlib.Path.home(), 'Pictures/photos')
PHOTOS_DIR = '/tmp/gcsphotos'
VIDEOS_DIR = '/tmp/gcs'
#  VIDEOS_DIR = os.path.join(pathlib.Path.home(), 'Videos/videos')

RAW_EXTENSIONS = ['ARW', 'CR2']
JPG_EXTENSIONS = ['JPG', 'JPEG']
VID_EXTENSIONS = ['MP4', 'MOV']

SD_ROOT = ''
EVENT_NAME = None
DRY_RUN = False
EXTERNAL = False

def parse_args():
    global EVENT_NAME
    global SD_ROOT
    global DRY_RUN
    global EXTERNAL
    parser = argparse.ArgumentParser()
    parser.add_argument('-e', '--event-name')
    parser.add_argument('-d', '--dry-run')
    parser.add_argument('-x', '--external')
    parser.add_argument('sd_root')
    args = parser.parse_args()
    EVENT_NAME = args.event_name
    SD_ROOT = args.sd_root
    DRY_RUN = bool(args.dry_run)
    EXTERNAL = bool(args.external)


def get_video_date(filepath):
    exiftool = subprocess.run([
        'exiftool',
        '-CreationDateValue',
        '-MediaCreateDate',
        '-api',
        'largefilesupport=1',
        '-j',
        filepath
    ], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    resp = json.loads(exiftool.stdout)

    if 'CreationDateValue' in resp[0] and '0000' not in resp[0]['CreationDateValue']:
        tm = datetime.datetime.strptime(resp[0]['CreationDateValue'], '%Y:%m:%d %H:%M:%S%z')
    elif 'MediaCreateDate' in resp[0] and '0000' not in resp[0]['MediaCreateDate']:
        tm = datetime.datetime.strptime(resp[0]['MediaCreateDate'], '%Y:%m:%d %H:%M:%S')
    else:
        tm = datetime.datetime.fromtimestamp(0)

    return tm


def get_created_date(filepath):
    exiftool = subprocess.run([
        'exiftool',
        '-CreateDate',
        '-FileModifyDate',
        '-DateTimeOriginal',
        '-j',
        filepath
    ], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    resp = json.loads(exiftool.stdout)

    if 'DateTimeOriginal' in resp[0] and '0000' not in resp[0]['DateTimeOriginal']:
        tm = datetime.datetime.strptime(resp[0]['DateTimeOriginal'], '%Y:%m:%d %H:%M:%S')
    elif 'CreateDate' in resp[0] and '0000' not in resp[0]['CreateDate']:
        tm = datetime.datetime.strptime(resp[0]['CreateDate'], '%Y:%m:%d %H:%M:%S')
    #  elif 'FileModifyDate' in resp[0] and '0000' not in resp[0]['FileModifyDate']:
        #  tm = datetime.datetime.strptime(resp[0]['FileModifyDate'], '%Y:%m:%d %H:%M:%S%z')
    else:
        tm = datetime.datetime.fromtimestamp(0)

    return tm


def create_dest_folder(root, dt, subFolders):
    destdir = '{}/{:02d}-{}/{}-{}'.format(dt.year, dt.month, dt.strftime("%B"), dt.day, dt.strftime("%b"))

    if EVENT_NAME:
        destdir += '-{}'.format(EVENT_NAME)

    for f in subFolders:
        pathlib.Path(os.path.join(root, destdir, f)).mkdir(parents=True, exist_ok=True)

    return os.path.join(root, destdir)


def create_dest_filename(dt, extension):
    dest = '{}{:02d}{:02d}-{:02d}{:02d}{:02d}'.format(dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second)

    if EVENT_NAME:
        dest += '-{}'.format(EVENT_NAME)
    dest += extension.upper()

    return dest


def do_copy(src, dest):
    cmd = ['rsync', '-LKvr']

    if DRY_RUN:
        cmd.append('--dry-run')
    subprocess.run(cmd + [src, dest])


def copy_image_file(src_path):
    dt = get_created_date(src_path)
    (_, ext) = os.path.splitext(src_path)

    dest_folders = ['RAW','JPG','EDIT']

    if EXTERNAL or '/EXT/' in src_path:
        dest_folders.append('EXT')

    dest = create_dest_folder(PHOTOS_DIR, dt, dest_folders)
    dest_filename = create_dest_filename(dt, ext)

    dest_type = 'RAW'

    ext = ext.replace('.', '')

    if ext.upper() in JPG_EXTENSIONS:
        dest_type = 'JPG'

    if EXTERNAL or '/EXT/' in src_path:
        dest_type = 'EXT'

    do_copy(src_path, os.path.join(dest, dest_type, dest_filename))


def copy_video_file(src_path):
    dt = get_video_date(src_path)
    (_, ext) = os.path.splitext(src_path)
    dest = create_dest_folder(VIDEOS_DIR, dt, ['RAW'])
    dest_filename = create_dest_filename(dt, ext)

    do_copy(src_path, os.path.join(dest, 'RAW', dest_filename))

    if os.path.isfile(src_path.replace('.MP4', 'M01.XML')):
        do_copy(src_path.replace('.MP4', 'M01.XML'), os.path.join(dest, 'RAW', dest_filename.replace(ext,'.XML')))


def walk():
    for path, dirnames, filenames in os.walk(SD_ROOT):

        for f in filenames:
            if f.startswith('.'):
                continue

            fullpath = os.path.abspath(os.path.join(path, f))

            if 'THMBNL' in fullpath:
                continue

            (_, ext) = os.path.splitext(fullpath)
            ext = ext.replace('.', '')

            if ext.upper() in JPG_EXTENSIONS or ext.upper() in RAW_EXTENSIONS:
                copy_image_file(fullpath)

            if ext.upper() in VID_EXTENSIONS:
                copy_video_file(fullpath)


def main():
    parse_args()
    walk()


if __name__ == '__main__':
    main()

