alias ..='cd ..'
alias ....='cd ../..'

alias dotfiles='cd ~/.dotfiles'

alias h='history'

if type exa > /dev/null; then
  alias l='exa'
  alias ls='exa'
  alias ll='exa -l'
  alias la='exa -al'
  alias lh='exa -alh'
  alias lt='exa -T'
  alias llfu='exa -bghHliS --git'
  alias tree='exa --tree'
else
  alias l='ls -lFh'     #size,show type,human readable
  alias la='ls -lAFh'   #long list,show almost all,show type,human readable
  alias lr='ls -tRFh'   #sorted by date,recursive,show type,human readable
  alias lt='ls -ltFh'   #long list,sorted by date,show type,human readable
  alias ll='ls -l'      #long list
  alias ldot='ls -ld .*'
  alias lS='ls -1FSsh'
  alias lart='ls -1Fcart'
  alias lrt='ls -1Fcrt'
fi

alias rla='. ~/.bashrc'

alias docker-less='docker ps | less -SEX'
alias docker-kill='docker kill $(docker ps -q)'
alias docker-rm='docker rm $(docker ps -a -q)'
alias docker-rmi='docker rmi $(docker images -q)'
alias docker-rmv='docker volume ls -qf dangling=true | xargs -r docker volume rm'
alias dockers='docker ps --format "table {{.Names}}\\t{{.Image}}\\t{{.Status}}\\t{{.Ports}}\\t{{.Command}}"'

alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'

alias dud='du -d 1 -h'
alias duf='du -sh *'

alias t='tail -f'

